package com.clearos.clearnames.dialogs

import androidx.appcompat.app.AlertDialog
import com.clearos.app.commons.activities.BaseSimpleActivity
import com.clearos.app.commons.extensions.setupDialogStuff
import com.clearos.app.commons.helpers.TAB_CONTACTS
import com.clearos.app.commons.helpers.TAB_FAVORITES
import com.clearos.app.commons.helpers.TAB_GROUPS
import com.clearos.app.commons.views.MyAppCompatCheckbox
import com.clearos.clearnames.R
import com.clearos.clearnames.extensions.config
import com.clearos.clearnames.helpers.ALL_TABS_MASK

class ManageVisibleTabsDialog(val activity: BaseSimpleActivity) {
    private var view = activity.layoutInflater.inflate(R.layout.dialog_manage_visible_tabs, null)
    private val tabs = LinkedHashMap<Int, Int>()

    init {
        tabs.apply {
            put(TAB_CONTACTS, R.id.manage_visible_tabs_contacts)
            put(TAB_FAVORITES, R.id.manage_visible_tabs_favorites)
            put(TAB_GROUPS, R.id.manage_visible_tabs_groups)
        }

        val showTabs = activity.config.showTabs
        for ((key, value) in tabs) {
            view.findViewById<MyAppCompatCheckbox>(value).isChecked = showTabs and key != 0
        }

        AlertDialog.Builder(activity)
            .setPositiveButton(R.string.ok) { dialog, which -> dialogConfirmed() }
            .setNegativeButton(R.string.cancel, null)
            .create().apply {
                activity.setupDialogStuff(view, this)
            }
    }

    private fun dialogConfirmed() {
        var result = 0
        for ((key, value) in tabs) {
            if (view.findViewById<MyAppCompatCheckbox>(value).isChecked) {
                result += key
            }
        }

        if (result == 0) {
            result = ALL_TABS_MASK
        }

        activity.config.showTabs = result
    }
}
