package com.clearos.clearnames.dialogs

import android.view.View
import androidx.appcompat.app.AlertDialog
import com.clearos.app.commons.activities.BaseSimpleActivity
import com.clearos.app.commons.dialogs.RadioGroupDialog
import com.clearos.app.commons.extensions.setupDialogStuff
import com.clearos.app.commons.extensions.showKeyboard
import com.clearos.app.commons.extensions.toast
import com.clearos.app.commons.extensions.value
import com.clearos.app.commons.helpers.ensureBackgroundThread
import com.clearos.app.commons.models.RadioItem
import com.clearos.clearnames.R
import com.clearos.clearnames.extensions.getPrivateContactSource
import com.clearos.clearnames.helpers.ContactsHelper
import com.clearos.clearnames.models.ContactSource
import com.clearos.clearnames.models.Group
import kotlinx.android.synthetic.main.dialog_create_new_group.view.*

class CreateNewGroupDialog(val activity: BaseSimpleActivity, val callback: (newGroup: Group) -> Unit) {
    init {
        val view = activity.layoutInflater.inflate(R.layout.dialog_create_new_group, null)

        AlertDialog.Builder(activity)
                .setPositiveButton(R.string.ok, null)
                .setNegativeButton(R.string.cancel, null)
                .create().apply {
                    activity.setupDialogStuff(view, this, R.string.create_new_group) {
                        showKeyboard(view.group_name)
                        getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(View.OnClickListener {
                            val name = view.group_name.value
                            if (name.isEmpty()) {
                                activity.toast(R.string.empty_name)
                                return@OnClickListener
                            }

                            val contactSources = ArrayList<ContactSource>()
                            ContactsHelper(activity).getContactSources {
                                it.filter { it.type.contains("google", true) }.mapTo(contactSources) { ContactSource(it.name, it.type, it.name) }
                                contactSources.add(activity.getPrivateContactSource())

                                val items = ArrayList<RadioItem>()
                                contactSources.forEachIndexed { index, contactSource ->
                                    items.add(RadioItem(index, contactSource.publicName))
                                }

                                activity.runOnUiThread {
                                    if (items.size == 1) {
                                        createGroupUnder(name, contactSources.first(), this)
                                    } else {
                                        RadioGroupDialog(activity, items, titleId = R.string.create_group_under_account) {
                                            val contactSource = contactSources[it as Int]
                                            createGroupUnder(name, contactSource, this)
                                        }
                                    }
                                }
                            }
                        })
                    }
                }
    }

    private fun createGroupUnder(name: String, contactSource: ContactSource, dialog: AlertDialog) {
        ensureBackgroundThread {
            val newGroup = ContactsHelper(activity).createNewGroup(name, contactSource.name, contactSource.type)
            activity.runOnUiThread {
                if (newGroup != null) {
                    callback(newGroup)
                }
                dialog.dismiss()
            }
        }
    }
}
