package com.clearos.clearnames.dialogs

import androidx.appcompat.app.AlertDialog
import com.clearos.app.commons.activities.BaseSimpleActivity
import com.clearos.app.commons.extensions.setupDialogStuff
import com.clearos.app.commons.extensions.showKeyboard
import com.clearos.app.commons.extensions.toast
import com.clearos.app.commons.extensions.value
import com.clearos.clearnames.R
import kotlinx.android.synthetic.main.dialog_custom_label.view.*

class CustomLabelDialog(val activity: BaseSimpleActivity, val callback: (label: String) -> Unit) {
    init {

        val view = activity.layoutInflater.inflate(R.layout.dialog_custom_label, null)

        AlertDialog.Builder(activity)
                .setPositiveButton(R.string.ok, null)
                .setNegativeButton(R.string.cancel, null)
                .create().apply {
                    activity.setupDialogStuff(view, this, R.string.label) {
                        showKeyboard(view.custom_label_edittext)
                        getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener {
                            val label = view.custom_label_edittext.value
                            if (label.isEmpty()) {
                                activity.toast(R.string.empty_name)
                                return@setOnClickListener
                            }

                            callback(label)
                            dismiss()
                        }
                    }
                }
    }
}
