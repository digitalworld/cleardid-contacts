package com.clearos.clearnames.activities

import android.app.AlarmManager
import android.app.PendingIntent
import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.View
import com.clearos.app.commons.dialogs.RadioGroupDialog
import com.clearos.app.commons.extensions.*
import com.clearos.app.commons.helpers.*
import com.clearos.app.commons.models.RadioItem
import com.clearos.clearnames.R
import com.clearos.clearnames.dialogs.ManageVisibleFieldsDialog
import com.clearos.clearnames.dialogs.ManageVisibleTabsDialog
import com.clearos.clearnames.extensions.config
import com.clearos.clearnames.helpers.ON_CLICK_CALL_CONTACT
import com.clearos.clearnames.helpers.ON_CLICK_EDIT_CONTACT
import com.clearos.clearnames.helpers.ON_CLICK_VIEW_CONTACT
import com.clearos.clearnames.services.BackupService
import com.clearos.updates.InAppUpdateActivity
import kotlinx.android.synthetic.main.activity_settings.*
import org.joda.time.Days
import java.util.*
import java.util.concurrent.TimeUnit

class SettingsActivity : SimpleActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        showTransparentTop = true
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_settings)
        settings_scrollview.systemUiVisibility = View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
    }

    override fun onResume() {
        super.onResume()

        setupCustomizeColors()
        setupManageShownContactFields()
        setupManageShownTabs()
        setupFontSize()
        setupBackupReminder()
        setupBackupInterval()
        setupUseEnglish()
        setupShowContactThumbnails()
        setupShowPhoneNumbers()
        setupShowContactsWithNumbers()
        setupStartNameWithSurname()
        setupShowCallConfirmation()
        setupShowDialpadButton()
        setupShowPrivateContacts()
        setupOnContactClick()
        setupCheckForUpdates()
        setupDefaultTab()
        updateTextColors(settings_holder)
        invalidateOptionsMenu()

        arrayOf(settings_color_customization_label, settings_general_settings_label, settings_backup_label, settings_main_screen_label, settings_list_view_label).forEach {
            it.setTextColor(getAdjustedPrimaryColor())
        }

        arrayOf(
            settings_color_customization_holder,
            settings_general_settings_holder,
            settings_main_screen_holder,
            settings_list_view_holder
        ).forEach {
            it.background.applyColorFilter(baseConfig.backgroundColor.getContrastColor())
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        updateMenuItemColors(menu)
        return super.onCreateOptionsMenu(menu)
    }

    private fun setupCustomizeColors() {
        settings_customize_colors_holder.setOnClickListener {
            startCustomizationActivity()
        }
    }

    private fun setupManageShownContactFields() {
        settings_manage_contact_fields_holder.setOnClickListener {
            ManageVisibleFieldsDialog(this) {}
        }
    }

    private fun setupManageShownTabs() {
        settings_manage_shown_tabs_holder.setOnClickListener {
            ManageVisibleTabsDialog(this)
        }
    }

    private fun setupDefaultTab() {
        settings_default_tab.text = getDefaultTabText()
        settings_default_tab_holder.setOnClickListener {
            val items = arrayListOf(
                RadioItem(TAB_CONTACTS, getString(R.string.contacts_tab)),
                RadioItem(TAB_FAVORITES, getString(R.string.favorites_tab)),
                RadioItem(TAB_GROUPS, getString(R.string.groups_tab)),
                RadioItem(TAB_LAST_USED, getString(R.string.last_used_tab))
            )

            RadioGroupDialog(this@SettingsActivity, items, config.defaultTab) {
                config.defaultTab = it as Int
                settings_default_tab.text = getDefaultTabText()
            }
        }
    }

    private fun getDefaultTabText() = getString(
        when (baseConfig.defaultTab) {
            TAB_CONTACTS -> R.string.contacts_tab
            TAB_FAVORITES -> R.string.favorites_tab
            TAB_GROUPS -> R.string.groups_tab
            else -> R.string.last_used_tab
        }
    )

    private fun setupFontSize() {
        settings_font_size.text = getFontSizeText()
        settings_font_size_holder.setOnClickListener {
            val items = arrayListOf(
                RadioItem(FONT_SIZE_SMALL, getString(R.string.small)),
                RadioItem(FONT_SIZE_MEDIUM, getString(R.string.medium)),
                RadioItem(FONT_SIZE_LARGE, getString(R.string.large)),
                RadioItem(FONT_SIZE_EXTRA_LARGE, getString(R.string.extra_large))
            )

            RadioGroupDialog(this@SettingsActivity, items, config.fontSize) {
                config.fontSize = it as Int
                settings_font_size.text = getFontSizeText()
            }
        }
    }

    private fun setupBackupReminder() {
        settings_backup_reminder_dialog.isChecked = config.backupReminder
        settings_backup_reminder_holder.setOnClickListener {
            settings_backup_reminder_dialog.toggle()
            config.backupReminder = settings_backup_reminder_dialog.isChecked
        }
    }

    private fun setupBackupInterval() {
        settings_backup_interval.text = getBackupInterval()
        settings_backup_interval_holder.setOnClickListener {
            val items = arrayListOf(
                RadioItem(BACKUP_NEVER, getString(R.string.never)),
                RadioItem(BACKUP_DAILY, getString(R.string.daily)),
                RadioItem(BACKUP_WEEKLY, getString(R.string.weekly)),
                RadioItem(BACKUP_MONTHLY, getString(R.string.monthly))
            )

            RadioGroupDialog(this@SettingsActivity, items, config.backupInterval) {
                config.backupInterval = it as Int
                settings_backup_interval.text = getBackupInterval()
                updateAutoBackupAlarm()
            }
        }
    }

    private fun setupUseEnglish() {
        settings_use_english_holder.beVisibleIf(config.wasUseEnglishToggled || Locale.getDefault().language != "en")
        settings_use_english.isChecked = config.useEnglish

        if (settings_use_english_holder.isGone()) {
            settings_font_size_holder.background = resources.getDrawable(R.drawable.ripple_top_corners, theme)
        }

        settings_use_english_holder.setOnClickListener {
            settings_use_english.toggle()
            config.useEnglish = settings_use_english.isChecked
            System.exit(0)
        }
    }

    private fun setupShowContactThumbnails() {
        settings_show_contact_thumbnails.isChecked = config.showContactThumbnails
        settings_show_contact_thumbnails_holder.setOnClickListener {
            settings_show_contact_thumbnails.toggle()
            config.showContactThumbnails = settings_show_contact_thumbnails.isChecked
        }
    }

    private fun setupShowPhoneNumbers() {
        settings_show_phone_numbers.isChecked = config.showPhoneNumbers
        settings_show_phone_numbers_holder.setOnClickListener {
            settings_show_phone_numbers.toggle()
            config.showPhoneNumbers = settings_show_phone_numbers.isChecked
        }
    }

    private fun setupShowContactsWithNumbers() {
        settings_show_only_contacts_with_numbers.isChecked = config.showOnlyContactsWithNumbers
        settings_show_only_contacts_with_numbers_holder.setOnClickListener {
            settings_show_only_contacts_with_numbers.toggle()
            config.showOnlyContactsWithNumbers = settings_show_only_contacts_with_numbers.isChecked
        }
    }

    private fun setupStartNameWithSurname() {
        settings_start_name_with_surname.isChecked = config.startNameWithSurname
        settings_start_name_with_surname_holder.setOnClickListener {
            settings_start_name_with_surname.toggle()
            config.startNameWithSurname = settings_start_name_with_surname.isChecked
        }
    }

    private fun setupShowDialpadButton() {
        settings_show_dialpad_button.isChecked = config.showDialpadButton
        settings_show_dialpad_button_holder.setOnClickListener {
            settings_show_dialpad_button.toggle()
            config.showDialpadButton = settings_show_dialpad_button.isChecked
        }
    }

    private fun setupShowPrivateContacts() {
        settings_show_private_contacts.isChecked = config.showPrivateContacts
        settings_show_private_contacts_holder.setOnClickListener {
            settings_show_private_contacts.toggle()
            config.showPrivateContacts = settings_show_private_contacts.isChecked
        }
    }

    private fun setupOnContactClick() {
        settings_on_contact_click.text = getOnContactClickText()
        settings_on_contact_click_holder.setOnClickListener {
            val items = arrayListOf(
                RadioItem(ON_CLICK_CALL_CONTACT, getString(R.string.call_contact)),
                RadioItem(ON_CLICK_VIEW_CONTACT, getString(R.string.view_contact)),
                RadioItem(ON_CLICK_EDIT_CONTACT, getString(R.string.edit_contact))
            )

            RadioGroupDialog(this@SettingsActivity, items, config.onContactClick) {
                config.onContactClick = it as Int
                settings_on_contact_click.text = getOnContactClickText()
            }
        }
    }

    private fun getOnContactClickText() = getString(
        when (config.onContactClick) {
            ON_CLICK_CALL_CONTACT -> R.string.call_contact
            ON_CLICK_VIEW_CONTACT -> R.string.view_contact
            else -> R.string.edit_contact
        }
    )

    private fun setupShowCallConfirmation() {
        settings_show_call_confirmation.isChecked = config.showCallConfirmation
        settings_show_call_confirmation_holder.setOnClickListener {
            settings_show_call_confirmation.toggle()
            config.showCallConfirmation = settings_show_call_confirmation.isChecked
        }
    }

    private fun setupCheckForUpdates() {
        settings_check_for_updates_holder.setOnClickListener {
            val intent = InAppUpdateActivity.getCheckForUpdatesIntent(
                this@SettingsActivity,
                SplashActivity::class.java
            )
            startActivity(intent)
        }
    }

    private fun updateAutoBackupAlarm() {
        val am = getSystemService(ALARM_SERVICE) as AlarmManager
        val i = Intent(applicationContext, BackupService::class.java)
        val pendingIntent = PendingIntent.getService(applicationContext, 0, i, PendingIntent.FLAG_UPDATE_CURRENT)

        val interval = when (config.backupInterval) {
            BACKUP_DAILY -> TimeUnit.MINUTES.toMillis(10)
            BACKUP_WEEKLY -> TimeUnit.DAYS.toMillis(7)
            BACKUP_MONTHLY -> TimeUnit.DAYS.toMillis(30)
            else -> 0
        }

        if (interval != 0L) {
            am.setRepeating(AlarmManager.RTC, System.currentTimeMillis(), interval, pendingIntent)
        } else {
            am.cancel(pendingIntent)
        }
    }
}
