package com.clearos.clearnames.interfaces;

import ezvcard.io.StreamWriter;

public interface Uploader {
    StreamWriter getWriter();
    String getCid();
    String getKey();
    void start();
    int getBlockId();
    long getBlockLength();
}

