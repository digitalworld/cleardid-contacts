package com.clearos.clearnames.interfaces;

import ezvcard.io.StreamReader;

public interface Downloader {
    StreamReader getReader();
    String getKey();
    String getCid();
    int getBlockId();
    void start();
}

