package com.clearos.app.commons.interfaces

interface LineColorPickerListener {
    fun colorChanged(index: Int, color: Int)
}
