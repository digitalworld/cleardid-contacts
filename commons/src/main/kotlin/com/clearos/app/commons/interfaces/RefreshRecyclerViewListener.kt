package com.clearos.app.commons.interfaces

interface RefreshRecyclerViewListener {
    fun refreshItems()
}
