package com.clearos.app.commons.interfaces

interface RecyclerScrollCallback {
    fun onScrolled(scrollY: Int)
}
